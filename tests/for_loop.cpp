#include "catch.hpp"

#include <sstream>
#include "cpl_tmpl/template.h"

// Test integration with nlohmann/json

namespace {
std::string render_to_string(const cpl_tmpl::Template& tmpl,
                             const cpl_tmpl::Context& ctx) {
  std::ostringstream dst;
  tmpl.render(dst, ctx);
  return dst.str();
}
}  // namespace

TEST_CASE("simple for_loop", "[for]") {
  cpl_tmpl::Interp_template tmpl("{% for x in vals %}{{x}}{%endfor%}");
  cpl_tmpl::Context ctx;

  std::vector<std::string> vals = {"a", "b", "c"};

  ctx.add_root("vals", vals);

  REQUIRE(render_to_string(tmpl, ctx) == "abc");
}
