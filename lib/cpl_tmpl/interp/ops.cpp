#include "cpl_tmpl/interp/ops.h"

namespace cpl_tmpl {
Print_raw_op::Print_raw_op(std::string_view data) : data_(std::move(data)) {}

void Print_raw_op::render(Stream_t& dst, const Context_api& ctx) const {
  dst << data_;
}

Print_op::Print_op(std::unique_ptr<ast::Expr> expr) : expr_(std::move(expr)) {}

void Print_op::render(Stream_t& dst, const Context_api& ctx) const {
  auto evaled = expr_->eval(ctx);

  evaled.get(dst);
}

class For_ctx : public Context_api {
 public:
  For_ctx(const Context_api& parent, std::string_view var_name)
      : parent_(parent), var_name_(var_name) {}

  std::optional<Data_provider> lookup(
      const std::string_view& name) const override {
    if (name == var_name_) {
      return data_;
    }
    return parent_.lookup(name);
  }

  const Context_api& parent_;
  std::string_view var_name_;
  Data_provider data_;
};

For_op::For_op(std::string_view var_name, std::unique_ptr<ast::Expr> expr,
               std::vector<std::unique_ptr<Template_op>> ops)
    : var_name_(var_name), expr_(std::move(expr)), ops_(std::move(ops)) {}

void For_op::render(Stream_t& dst, const Context_api& ctx) const {
  auto list = expr_->eval(ctx);

  For_ctx sub_context(ctx, var_name_);

  list.visit([&](Data_provider d) {
    sub_context.data_ = d;
    for (const auto& i : ops_) {
      i->render(dst, sub_context);
    }
  });
}

}  // namespace cpl_tmpl