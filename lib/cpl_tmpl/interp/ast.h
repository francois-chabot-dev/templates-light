#ifndef CPL_IMPL_AST_H
#define CPL_IMPL_AST_H

#include "cpl_tmpl/data_provider.h"
#include "cpl_tmpl/template.h"

#include <string_view>

namespace cpl_tmpl {

namespace ast {
class Expr {
 public:
  virtual ~Expr() {}
  virtual Data_provider eval(const Context_api& ctx) const = 0;
};

using Expr_ptr = std::unique_ptr<Expr>;

// Identifier
class Id_ref final : public Expr {
 public:
  Id_ref(std::string_view);

  Data_provider eval(const Context_api& ctx) const override;

 private:
  std::string_view id_;
};

// <Expr>.Identifier
class Resolve final : public Expr {
 public:
  Resolve(Expr_ptr, std::string_view);
  Data_provider eval(const Context_api& ctx) const override;

 private:
  Expr_ptr lhs_;
  std::string_view key_;
};

// <Expr> |Identifier
class Filter final : public Expr {
 public:
  Data_provider eval(const Context_api& ctx) const override {
    return Data_provider();
  }
};

class String_literal final : public Expr {
 public:
  String_literal(std::string_view data);
  Data_provider eval(const Context_api& ctx) const override;

 private:
  std::string_view data_;
};
}  // namespace ast
}  // namespace cpl_tmpl

#endif