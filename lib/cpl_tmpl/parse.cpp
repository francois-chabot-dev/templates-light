#include "cpl_tmpl/interp/ops.h"
#include "cpl_tmpl/lex.h"
#include "cpl_tmpl/template.h"

#include <algorithm>
#include <cassert>
#include <functional>

namespace cpl_tmpl {

std::vector<std::unique_ptr<Template_op>> parse(std::string_view::iterator& ite,
                                                std::string_view::iterator end,
                                                std::string_view end_directive);

namespace {

using Expr_ptr = std::unique_ptr<ast::Expr>;

Expr_ptr parse_expr(std::string_view raw);
Expr_ptr parse_expr(Token_iterator& b, Token_iterator e);
Expr_ptr parse_primary(Token_iterator& b, Token_iterator e);
Expr_ptr parse_parens(Token_iterator& b, Token_iterator e);

Expr_ptr parse_primary(Token_iterator& b, Token_iterator e) {
  Expr_ptr result;
  switch (b->type) {
    case Token_t::identifier:
      result = std::make_unique<ast::Id_ref>(b->raw);
      ++b;
      break;
    case Token_t::number:
      break;
    case Token_t::parens_open:
      result = parse_parens(b, e);
      break;
    default:
      break;
  }
  return result;
}

Expr_ptr parse_parens(Token_iterator& b, Token_iterator e) {
  ++b;
  auto result = parse_expr(b, e);
  if (b->type != Token_t::parens_close) {
    throw std::runtime_error("expecting )");
  }
  ++b;
  return result;
}

Expr_ptr parse_expr(std::string_view raw) {
  Token_iterator b(raw);

  return parse_expr(b, Token_iterator());
}

Expr_ptr parse_expr(Token_iterator& b, Token_iterator e) {
  if (b == e) {
    return nullptr;
  }

  auto result = parse_primary(b, e);

  while (b != e) {
    switch (b->type) {
      case Token_t::period:
        ++b;
        if (b == e || b->type != Token_t::identifier) {
          throw std::runtime_error("expecting identifier");
        }
        result = std::make_unique<ast::Resolve>(std::move(result), b->raw);
        ++b;
        break;
      default:
        throw std::runtime_error("extremely confused");
    }
  }

  return result;
}

}  // namespace

std::unique_ptr<Template_op> handle_for_directive(
    Token_iterator& tag_ite, Token_iterator tag_end,
    std::string_view::iterator& ite, std::string_view::iterator end) {
  if (tag_ite == tag_end || tag_ite->type != Token_t::identifier) {
    throw std::runtime_error("expecting identifier");
  }

  auto iter_name = tag_ite->raw;
  ++tag_ite;

  if (tag_ite == tag_end || tag_ite->raw != "in") {
    throw std::runtime_error("expecting 'in'");
  }
  ++tag_ite;

  auto iter_src = parse_expr(tag_ite, tag_end);

  if (!iter_src) {
    throw std::runtime_error("expecting expression");
  }

  if (tag_ite != tag_end) {
    throw std::runtime_error("expecting end of tag");
  }

  return std::make_unique<For_op>(iter_name, std::move(iter_src),
                                  parse(ite, end, "endfor"));
}

std::unique_ptr<Template_op> handle_if_directive(
    Token_iterator& tag_begin, Token_iterator tag_end,
    std::string_view::iterator& ite, std::string_view::iterator end) {
  return nullptr;
}

std::unique_ptr<Template_op> handle_print(std::string_view::iterator& ite,
                                          std::string_view::iterator end) {
  const std::string_view tag_end_str = "}}";
  std::advance(ite, 2);
  auto tag_end = std::search(
      ite, end,
      std::boyer_moore_searcher(tag_end_str.begin(), tag_end_str.end()));

  if (tag_end == end) {
    throw std::runtime_error("unterminated tag");
  }

  std::string_view expr_string(&*ite, tag_end - ite);
  ite = std::next(tag_end, 2);

  return std::make_unique<Print_op>(parse_expr(expr_string));
}

std::tuple<bool, std::unique_ptr<Template_op>> handle_directive(
    std::string_view::iterator& ite, std::string_view::iterator end,
    std::string_view end_directive) {
  const std::string_view tag_end_str = "%}";
  std::advance(ite, 2);
  auto tag_end = std::search(
      ite, end,
      std::boyer_moore_searcher(tag_end_str.begin(), tag_end_str.end()));

  if (tag_end == end) {
    throw std::runtime_error("unterminated tag");
  }

  std::string_view directive_string(&*ite, tag_end - ite);
  ite = std::next(tag_end, 2);

  Token_iterator b(directive_string);
  Token_iterator e;

  if (b->type != Token_t::identifier) {
    throw std::runtime_error("expecting identifier");
  }

  if (b->raw == "for") {
    ++b;
    return std::make_tuple(false, handle_for_directive(b, e, ite, end));
  } else if (b->raw == "if") {
    ++b;
    return std::make_tuple(false, handle_if_directive(b, e, ite, end));
  } else if (!end_directive.empty() && b->raw == end_directive) {
    return std::make_tuple(true, nullptr);
  }
  throw std::runtime_error("unknown directive");
}

std::vector<std::unique_ptr<Template_op>> parse(
    std::string_view::iterator& ite, std::string_view::iterator end,
    std::string_view end_directive) {
  using Ite = std::string_view::iterator;

  std::vector<std::unique_ptr<Template_op>> result;

  auto raw_start = ite;
  auto consume_raw = [&](Ite raw_end) {
    if (raw_end != raw_start) {
      result.emplace_back(std::make_unique<Print_raw_op>(
          std::string_view(&*raw_start, raw_end - raw_start)));
      raw_start = raw_end;
    }
  };

  while (true) {
    // Advance to the next open brace
    ite = std::find(ite, end, '{');

    if (ite == end) break;

    auto next = std::next(ite);
    // Handle the last character being an open brace
    if (ite == end) break;

    switch (*next) {
      case '{':
        consume_raw(ite);
        result.emplace_back(handle_print(ite, end));
        raw_start = ite;
        break;
      case '%': {
        consume_raw(ite);
        auto [bail, act] = handle_directive(ite, end, end_directive);
        raw_start = ite;

        if (bail) {
          return result;
        }

        result.emplace_back(std::move(act));
      } break;
      default:
        ite = std::next(next);
        break;
    }
  }

  consume_raw(ite);
  return result;
}

std::vector<std::unique_ptr<Template_op>> parse(std::string_view text) {
  auto ite = text.begin();
  auto end = text.end();
  return parse(ite, end, std::string_view());
}
}  // namespace cpl_tmpl