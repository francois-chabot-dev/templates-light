#!/usr/bin/env bash

find . -iname *.cpp | xargs -n1 ./utils/invoke_clang_tidy.sh
