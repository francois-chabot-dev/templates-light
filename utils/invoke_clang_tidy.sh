#!/usr/bin/env bash

echo "Running clang-tidy on $1"
clang-tidy $1 -- -Iinclude/ -Ilib -std=c++17