#!/usr/bin/env bash

find . -iname *.h -o -iname *.cpp | xargs -n1 clang-format -style=file -i -verbose
