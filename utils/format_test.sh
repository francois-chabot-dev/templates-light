#!/usr/bin/env bash

# check that we are in a clean state in order to prevent accidential
# changes
cleanstate=`git status | grep "modified"`
if ! [[ -z $cleanstate ]]; then
  echo "Script must be applied on a clean git state"
  exit 1
fi

# Run the formatting script
./utils/format.sh

# check if something was modified
notcorrectlist=`git status | grep "modified"`

# if nothing changed ok
if [[ -z $notcorrectlist ]]; then
  # send a negative message to gitlab
  exit 0;
else
  echo "The following files have clang-format problems (showing patches)";
  for f in $notcorrectlist; do
      echo $f
      git diff $f
  done
fi

# cleanup changes in git
git reset HEAD --hard

exit 1
