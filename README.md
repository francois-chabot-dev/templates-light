# CPL_TMPL

A Jinja2-like template engine.

## Quick Start

```
#include "cpl_tmpl.h"

int main() {
  // Create a template
  cpl_tmpl::Interp_template tmpl("Hello, my name is {{name}}!");

  // Create a rendering context
  cpl_tmpl::Context ctx;

  // Populate the context
  ctx.add_root("name", "joe");

  // Render
  template.render(data, std::cout);

  return 0;
}
```
## Features
 
- Efficiency: Template rendering involves no copying. Raw data gets piped
              directly to the destination stream.
- Very close to Jinja2: The main difference is that redundant features are
                        not included.
- Unopinated: The default behavior is as simple and straight-forwards as 
              possible, with as few options as possible. 
- Extensible: Data sources and sinks are duck-typed on simple stl-like semantics,
              and that behavior can be easily overwritten.

## Usage

**N.B.** All features displayed in these examples do not 100% work yet.

### Using json data

Using [nlohmann/json](https://github.com/nlohmann/json), we can easily integrate
json data:

```
#include "cpl_tmpl.h"
#include "json.hpp"

using json = nlohmann::json;

// nlohmann::json is not quite stl-like in nature, so we need to overload
// a few helpers.
namespace cpl_tmpl {

template <>
struct Data_traits<json> : public Default_traits<json> {
  static constexpr bool is_value = true; // can be used in a {{var}}
  static constexpr bool is_sequence = true; // Can be used in a {% for ... %}
  static constexpr bool is_mapping = true; // Can be used in a {{obj.key}}

  // nlohmann/json adds quotation marks around strings when passing them
  // to a stream, so we need to access the underlying string instead.
  static void access(const json& src, cpl_tmpl::Stream_t& dst) {
    if (src.type() == json::value_t::string) {
      dst << src.get_ref<const json::string_t&>();
    } else {
      dst << src;
    }
  }
}

int main() {
  const char * raw_template = 
    "Hello, my name is {{name}}, and my friends are:"
    "{% for friend in friends %}"
    "  - {{friend.name}}"
    "{% endfor %}"

  // make a template
  auto template = cpl_tmpl::Interp_template(raw_template);

  // render the template:
  json data = {
    {"name" : "joe"},
    {"friends" : {
      {"name", "jack"}, 
      {"name", "mike"}, 
      {"name", "sue"}
      }
    }
  };

  template.render(data, std::cout);

  return 0;
};
```

## Roadmap

Version 0.1:
- {{var}}
- {% for val in range %}...{% endfor %}
- {% if condition %}...{% elif %}...{% else %}...{% endif %}
- {% include 'template' %}
- {% extends 'template' %}
- {% block block_name %}{% endblock %}
- {# comments #}
- filters
- {% raw %}... {% endraw %}

Future features:

- Native templates
  - Compile templates to C++ code.
  - Hot realoading of template library DLLs