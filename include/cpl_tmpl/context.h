#ifndef CPL_TMPL_CONTEXT_H
#define CPL_TMPL_CONTEXT_H

#include "cpl_tmpl/config.h"
#include "cpl_tmpl/data_provider.h"

#include <optional>
#include <unordered_map>

namespace cpl_tmpl {

class Context_api {
 public:
  virtual std::optional<Data_provider> lookup(
      const std::string_view& name) const = 0;

 protected:
  ~Context_api() {}
};

class Context : public Context_api {
 public:
  Context() = default;

  std::optional<Data_provider> lookup(
      const std::string_view& name) const override {
    auto found = data_roots_.find(name);
    if (found == data_roots_.end()) {
      return std::nullopt;
    }

    return found->second;
  }

  Data_provider& add_root(std::string_view key) {
    return data_roots_.emplace(key, Data_provider()).first->second;
  }

  template <typename T>
  Data_provider& add_root(std::string_view key, const T& data) {
    return add_root_provider(key, data_provider(data));
  }

  Data_provider& add_root_provider(std::string_view key, Data_provider data) {
    return data_roots_.emplace(key, data).first->second;
  }

 private:
  std::unordered_map<std::string_view, Data_provider> data_roots_;
};

}  // namespace cpl_tmpl
#endif